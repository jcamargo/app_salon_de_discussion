package controls;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.CChat;
import database.tools.*;
public class CInsertChatRoom extends HttpServlet {

	public void doPost(HttpServletRequest req , HttpServletResponse res) throws ServletException, IOException {
		CDAOChats daoChatRoom=new CDAOChats();		
		int duration=Integer.valueOf(req.getParameter("duration"));
		System.out.println("La duracion es de:"+duration);
		CChat newChatRoom= new CChat(
				req.getParameter("chatDescription"),
				req.getParameter("chatRoomName"),
				req.getParameter("date"),
				duration,
				String.valueOf(req.getSession().getAttribute("userEmail")),
				req.getParameter("startTime"),
				req.getParameter("endTime")
				);

		try {
			if(daoChatRoom.insert(newChatRoom)) {
			}
		} catch (ClassNotFoundException | IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		RequestDispatcher rd = req.getRequestDispatcher("mychatRooms.jsp");
		rd.forward(req, res);
		
	}
}
