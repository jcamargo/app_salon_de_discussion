package controls;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.tools.CDaoUsers;
import models.User;

public class CConnection extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		String email = String.valueOf(req.getParameter("email"));
		String pws = String.valueOf(req.getParameter("psw"));
		CDaoUsers daoUsers = new CDaoUsers();
		String redirectionPage = "";
		RequestDispatcher rd = null;

		if (daoUsers.valideCredentials(email, pws)) {
			HttpSession session = req.getSession();
			session.setAttribute("userEmail", email);
			User user;
			try {
			user = daoUsers.getUserById(email);
			session.setAttribute("userName", user.getName());
			session.setAttribute("userFamilyName", user.getFamilyName());
			redirectionPage = "mychatRooms.jsp";
			
				if(user.getRole().equals("admin")) {
					session.setAttribute("isAdmin", "true");
				}else {
					session.setAttribute("isAdmin", "false");
				}
			} catch (ClassNotFoundException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			redirectionPage = "login.jsp";
			
		}
		


		rd = req.getRequestDispatcher(redirectionPage);
		rd.forward(req, res);

	}
}
