package controls;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.tools.CDAOChats;

public class CDeleteChatRoom extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		//redirection vers la page d'accueil de l'utilisateur apres avoir efface le salon 
		int idChat = Integer.parseInt(req.getParameter("chatId"));
		CDAOChats daoChatRoom=new CDAOChats();	
		try {
			daoChatRoom.deleteChatRoom(idChat);
			RequestDispatcher rd = req.getRequestDispatcher("mychatRooms.jsp");
			rd.forward(req, res);
		} catch (ClassNotFoundException | SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
