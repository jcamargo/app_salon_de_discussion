package controls;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.tools.CDaoUsers;
import models.User;

public class CInsertUser extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException,IOException{
		
		User newUser = new User(String.valueOf(
				req.getParameter("name")),
				String.valueOf(req.getParameter("sourcename")), 
				String.valueOf(req.getParameter("email")),
				String.valueOf(req.getParameter("pws")),
				String.valueOf(req.getParameter("gender")),
				String.valueOf(req.getParameter("role")));
		CDaoUsers daoUsers = new CDaoUsers();
		
		try {
			daoUsers.insert(newUser);
			RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
			rd.forward(req, res);
		} catch (ClassNotFoundException | IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
