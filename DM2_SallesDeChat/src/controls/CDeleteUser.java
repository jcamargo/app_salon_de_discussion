package controls;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import database.tools.CDaoUsers;

import database.tools.CDAOChats;

public class CDeleteUser extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		String userEmail = String.valueOf(req.getParameter("userEmail"));
		CDaoUsers daoUsers=new CDaoUsers();	
		try {
			daoUsers.deleteUser(userEmail);
			RequestDispatcher rd = req.getRequestDispatcher("manageUsers.jsp");
			rd.forward(req, res);
		} catch (ClassNotFoundException | SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
