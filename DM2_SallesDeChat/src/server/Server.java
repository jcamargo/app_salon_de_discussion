package server;

import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.ServerEndpointConfig.Configurator;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import database.tools.CDaoUsers;
import java.util.concurrent.CopyOnWriteArraySet;
import models.User;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

@ServerEndpoint(value = "/chatRooms/{chatRoomName}/{login}")
public class Server {


    private static Map<String,CopyOnWriteArraySet<Server>> rooms= new HashMap();
    private Session sessionUser=null;
    private String userName="";
    private String chatRoomName="";
    private User myInformation=null;
    

	@OnOpen
	public void onOpen(Session session, @PathParam("login") String login,
			@PathParam("chatRoomName") String chatRoomName) throws ClassNotFoundException, SQLException, IOException {
		
		
		this.sessionUser=session;
		this.userName=login;
		this.chatRoomName=chatRoomName;
		CDaoUsers daoUsers = new CDaoUsers();
		String joinMessage="";
		CopyOnWriteArraySet<Server> listConnectedUsers=rooms.get(this.chatRoomName);
		if(listConnectedUsers==null) {
			synchronized(rooms) {
				if(!rooms.containsKey(this.chatRoomName)) {
					listConnectedUsers=new CopyOnWriteArraySet<Server>();
					rooms.put(this.chatRoomName, listConnectedUsers);
				}
			}
		}
		listConnectedUsers.add(this);
		myInformation = daoUsers.getUserById(this.userName);
		joinMessage=myInformation.getFamilyName()+", "+myInformation.getName()+" has joined to the conversation";
		receivingMessage( session,joinMessage );

	}

	@OnMessage
	public void receivingMessage(Session session, String message) {

		CopyOnWriteArraySet<Server> listConnectedUsers=rooms.get(this.chatRoomName);
		String messsageUser="";
		if(listConnectedUsers!=null) {
			for(Server user:listConnectedUsers) {
				if(message.contains("has joined to the conversation")) {
					messsageUser=message;
				}else {
					messsageUser=myInformation.getFamilyName()+", "+myInformation.getName()+": "+message;
				}

				user.sessionUser.getAsyncRemote().sendText(messsageUser);
			}
		}
		//System.out.println("User: "+this.userName+" ChatRoom:"+this.chatRoomName+" Message : " + message);
	}

	@OnClose
	public void closedConnection() {

		CopyOnWriteArraySet<Server> listConnectedUsers=rooms.get(this.chatRoomName);
		if(listConnectedUsers!=null) {

			listConnectedUsers.remove(this);
		}
	}
	
	public static  List <User> getNameConnectedUsers(String nameChatRoom) throws ClassNotFoundException, SQLException, IOException {
		String connectedUsers = "";
		System.out.println("recibi el valor"+nameChatRoom);
		CDaoUsers daoUser = new CDaoUsers();
		
		List <User> listUsers = new ArrayList<User>();
		Session tempSession = null;
		CopyOnWriteArraySet<Server> listConnectedUsers=rooms.get(nameChatRoom);
		for (Server user: listConnectedUsers) {
			listUsers.add(daoUser.getUserById(user.userName));
		}

		return listUsers;
	}
	

}
