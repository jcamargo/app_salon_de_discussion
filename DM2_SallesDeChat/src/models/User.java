package models;

public class User {

	private String name;
	private String familyName;
	private String email;
	private String password;
	private String gender;
	private String role;

	public User(String name, String familyName, String email, String password, String gender,String role) {
		this.setName(name);
		this.setFamilyName(familyName);
		this.setEmail(email);
		this.setPassword(password);
		this.setGender(gender);
		this.setRole(role);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
