package models;

public class CChat {

	private String emailOwner="";
	private  int idChat = 0;
	private String descriptionSalle;
	private String titleSalle;
	private String date;
	private String startTime;
	private String endTime;
	private int duree;
	

	public CChat() {

	}

	public CChat(int idChat,String descriptionSalle, String titleSalle, String date, int duree,String emailOwner,String startTime,String endTime) {
		this.idChat=idChat;
		this.descriptionSalle=descriptionSalle;
		this.titleSalle=titleSalle;
		this.date=date;
		this.duree=duree;
		this.setEmailOwner(emailOwner);
		this.startTime=startTime;
		this.endTime=endTime;
	}
	
	public CChat(String descriptionSalle, String titleSalle, String date, int duree,String emailOwner,String startTime,String endTime) {
		this.descriptionSalle=descriptionSalle;
		this.titleSalle=titleSalle;
		this.date=date;
		this.duree=duree;
		this.setEmailOwner(emailOwner);
		this.startTime=startTime;
		this.endTime=endTime;
	}



	public String getDescriptionSalle() {
		return descriptionSalle;
	}

	public void setDescriptionSalle(String descriptionSalle) {
		this.descriptionSalle = descriptionSalle;
	}

	public String getTitleSalle() {
		return titleSalle;
	}

	public void setTitleSalle(String titleSalle) {
		this.titleSalle = titleSalle;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public int getIdChat() {
		return idChat;
	}

	public void setIdChat(int idChat) {
		this.idChat = idChat;
	}

	public String getEmailOwner() {
		return emailOwner;
	}

	public void setEmailOwner(String emailOwner) {
		this.emailOwner = emailOwner;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
}
