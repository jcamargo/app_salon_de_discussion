package database.tools;

import java.io.IOException;
import models.User;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CDaoUsers {

	private ConfigConnection configConnection;
	public CDaoUsers() {
		this.configConnection=ConfigConnection.getInstance();
	}
	
	public boolean insert(User user) throws ClassNotFoundException, IOException, SQLException {
		String insertStatement="INSERT INTO users "
				+ "VALUES('"+user.getName()+"',"
				+ "'"+user.getFamilyName()+"',"
				+ "'"+user.getEmail()+"',"
				+ "'"+user.getRole()+"',"
				+ "'"+user.getPassword()+"',"
				+"'"+user.getGender()+"')";
		System.out.println(insertStatement);
		boolean transaction=false;
		Connection db=configConnection.getConnection();
		Statement sql = db.createStatement();
		sql.executeUpdate(insertStatement);

		return true;
	}
	
	public boolean valideCredentials(String email, String psw) {
		boolean userRegistered=false;
		String findUserById="SELECT * FROM users WHERE email='"+email+"'";
		Statement sql;
		try {
			sql = configConnection.getConnection().createStatement();
			ResultSet resultSet=sql.executeQuery(findUserById);
			if(resultSet!=null) {
				resultSet.next();
				if(email.equals(resultSet.getString("email")) && psw.equals(resultSet.getString("password")) ) {
					userRegistered=true;
				}
			}else {
				userRegistered=false;
			}
		} catch (ClassNotFoundException | SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return userRegistered;
	}
	

	
	public List<User> getAllUsers() throws ClassNotFoundException, SQLException, IOException {
		List <User> listUsers = new  ArrayList<User>();
		String searchUserQuery="SELECT *  FROM users";
		ResultSet  rs = configConnection.getConnection().createStatement().executeQuery(searchUserQuery);
		while (rs.next()) {
			listUsers.add(new User(rs.getString("name"),
					rs.getString("sourname"),
					rs.getString("email"),
					"",
					rs.getString("gender"),
					rs.getString("role")));
		}
		return listUsers;
	}
	
	public User getUserById(String email) throws ClassNotFoundException, SQLException, IOException {
		String searchUserQuery="SELECT *  FROM users WHERE email='"+email+"'";
		User user =null;
		ResultSet  rs = configConnection.getConnection().createStatement().executeQuery(searchUserQuery);
		while (rs.next()) {
			user = new User(rs.getString("name"),
					rs.getString("sourname"),
					rs.getString("email"),
					rs.getString("password"),
					rs.getString("gender"),
					rs.getString("role"));
		}
		return user;
	}
	public void  deleteUser(String email) throws ClassNotFoundException, SQLException, IOException {
		String deleteStamente="DELETE FROM users WHERE email='"+email+"'";
		if(configConnection.getConnection().createStatement().executeUpdate(deleteStamente)>0) {
		}
	}
	
	public ResultSet getUsersNotInvitedYet(String idChat,String email ) throws ClassNotFoundException, SQLException, IOException {
		ResultSet rs =null;
		//Modify this query , to only get the users who are not been invited to the chat

		String getUsersNotInvited= "SELECT a.name,a.sourname,a.email from users a LEFT JOIN userInvitedRooms c on c.email=a.email"
				+" and c.idChat="+idChat+" where c.email is null and c.idChat is null and  a.email !='"+email+"'";
		System.out.println("Se quiso ejecutar");
		System.out.println(getUsersNotInvited);
		Statement sql = configConnection.getConnection().createStatement();
		rs=sql.executeQuery(getUsersNotInvited);
		return rs;
	}
	
	public void updateUser(User userModified) throws ClassNotFoundException, IOException, SQLException {
		String updateStatement ="UPDATE users set name='"+userModified.getName()+
				"' , sourname='"+userModified.getFamilyName()+
				"', password='"+userModified.getPassword()+
				"',gender='"+userModified.getGender()+
				"', role='"+userModified.getRole()+
				"' WHERE email='"+userModified.getEmail()+"'";
		System.out.println(updateStatement);
		Connection db=configConnection.getConnection();
		Statement sql = db.createStatement();
		sql.executeUpdate(updateStatement);
	}
}
