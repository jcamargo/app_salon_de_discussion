package database.tools;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.CChat;
import models.User;
public class CDAOChats {

	private ConfigConnection configConnection;
	public CDAOChats() {
		this.configConnection=ConfigConnection.getInstance();
	}
	
	public boolean insert(CChat chat) throws ClassNotFoundException, IOException, SQLException {
		String insertStatement="INSERT INTO chatrooms (email,titleSalle,descriptionSalle,date,duration,startTime,endTime) "
				+ "VALUES('"+chat.getEmailOwner()+"',"
				+ "'"+chat.getTitleSalle()+"',"
				+ "'"+chat.getDescriptionSalle()+"',"
				+ "'"+chat.getDate()+"',"
				+ "'"+chat.getDuree()+"',"
				+ "'"+chat.getStartTime()+"',"
				+ "'"+chat.getEndTime()+"')";
		System.out.println(insertStatement);
		boolean transaction=false;
		Connection db=configConnection.getConnection();
		Statement sql = db.createStatement();
		sql.executeUpdate(insertStatement);
		
		return true;
	}
	

	public List<CChat> getAllMyChatRooms (String email) throws ClassNotFoundException, SQLException, IOException {
		List <CChat> listChatRooms = new  ArrayList<CChat>();
		String searchUserQuery="SELECT * FROM chatrooms WHERE email='"+email+"'";
		ResultSet  rs = configConnection.getConnection().createStatement().executeQuery(searchUserQuery);
		while (rs.next()) {
			listChatRooms.add(new CChat(
					rs.getInt("idChat"),
					rs.getString("descriptionSalle"),
					rs.getString("titleSalle"),
					rs.getString("date"),
					rs.getInt("duration"),
					rs.getString("email"),
					rs.getString("startTime"),
					rs.getString("endTime")));
		}
		return listChatRooms;
	}

	
	
	public List<CChat> getChatRoomsInvited(String email) throws SQLException, ClassNotFoundException, IOException {
		Connection db = configConnection.getConnection();
		Statement sql = db.createStatement();
		List <CChat> listChatRooms = new  ArrayList<CChat>();
		String findChatRooms="SELECT a.idChat,a.email,a.titleSalle,a.descriptionSalle,a.date,a.duration,a.startTime,a.endTime FROM chatrooms a INNER JOIN "
				+ "userInvitedRooms b ON a.idChat =b.idChat WHERE "
				+ "b.email='"+email+"'";
		System.out.println(findChatRooms);
		ResultSet rs =sql.executeQuery(findChatRooms);
		while (rs.next()) {
			listChatRooms.add(new CChat(
					rs.getInt("idChat"),
					rs.getString("descriptionSalle"),
					rs.getString("titleSalle"),
					rs.getString("date"),
					rs.getInt("duration"),
					rs.getString("email"),
					rs.getString("startTime"),
					rs.getString("endTime")));
		}
		return listChatRooms;
	}
	
	public boolean insertInvitedUser(String email,String chatRoomId) throws SQLException, ClassNotFoundException, IOException {
		Connection db = configConnection.getConnection();
		Statement sql = db.createStatement();
		String insertInvitedStatement="INSERT INTO userinvitedrooms "
				+"VALUES ('"+email+"',"+chatRoomId+")";
		System.out.println(insertInvitedStatement);
		sql.executeUpdate(insertInvitedStatement);
		
		return true;
	}
	
	public void deleteChatRoom(int idChat) throws ClassNotFoundException, SQLException, IOException {
		String deleteStamente="DELETE FROM chatrooms WHERE idChat="+idChat;
		if(configConnection.getConnection().createStatement().executeUpdate(deleteStamente)>0) {
			System.out.println("The element has been deleted from chatrooms table");
		}

	}
	public CChat getChatRoomById(String id) throws SQLException, ClassNotFoundException, IOException {
		String searchChatQuery="SELECT *  FROM chatrooms WHERE idChat="+id+"";
		CChat chat =null;
		ResultSet  rs = configConnection.getConnection().createStatement().executeQuery(searchChatQuery);
		while (rs.next()) {
			chat = new CChat(
					rs.getInt("idChat"),
					rs.getString("descriptionSalle"),
					rs.getString("titleSalle"),
					rs.getString("date"),
					rs.getInt("duration"),
					rs.getString("email"),
					rs.getString("startTime"),
					rs.getString("endTime"));
		}
		return chat;
	}
	
	public void update(CChat chat) throws SQLException, ClassNotFoundException, IOException {
		String updateChatRoom="UPDATE chatrooms SET "
				+ "descriptionSalle='"+chat.getDescriptionSalle()
				+ "',titleSalle='"+chat.getTitleSalle()
				+ "',date='"+chat.getDate()
				+ "',duration="+chat.getDuree()
				+ ",email='"+chat.getEmailOwner()
				+ "',startTime='"+chat.getStartTime()
				+ "',endTime='"+chat.getEndTime()
				+ "' WHERE idChat ="+chat.getIdChat(); 
		System.out.println(updateChatRoom);
		Connection db=configConnection.getConnection();
		Statement sql = db.createStatement();
		sql.executeUpdate(updateChatRoom);
		
	}
}
