package database.tools;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConfigConnection {
	private static ConfigConnection configConnection = null;

	private ConfigConnection() {
	}

	
	
public Connection getConnection() throws ClassNotFoundException, SQLException, IOException {
	 	
        Properties param = new Properties();
        URL urlFichierProp = ConfigConnection.class.getResource("props.properties");
        if (urlFichierProp == null) {
            throw new IOException("Fichier " + "BDparam" + " pas trouve !");
        }
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(urlFichierProp.openStream());
            param.load(bis);
            String driver = param.getProperty("driver");
            String url = param.getProperty("url");
            String utilisateur = param.getProperty("user");
            String mdp = param.getProperty("psw");
            Class.forName(driver);
            return (Connection) DriverManager.getConnection(url, utilisateur, mdp);
        } finally {
            if (bis != null) {
                bis.close();
            }
        }
		
	}
	/*public Connection getConnection(String nomFichierProp) throws IOException, ClassNotFoundException, SQLException {

		Class.forName("com.mysql.cj.jdbc.Driver");
		return (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/sr03td", "root", "batman&26S");

	}*/

	public static ConfigConnection getInstance() {
		if (configConnection == null) {
			configConnection = new ConfigConnection();
		}
		return configConnection;
	}

}
