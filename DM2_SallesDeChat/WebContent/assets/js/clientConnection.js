window.addEventListener
	("load", function(event) {

		let chatRoomName = document.getElementById("chatRoomName");
		let login = document.getElementById("login");
		let date = document.getElementById("dateChat");
		let hour = document.getElementById("endTimeChat");
		let mydate = date.value + " " + hour.value;

		//let ws = new WebSocket("ws://localhost:8084/DM2_SallesDeChat/chatRooms/"+chatRoomName.value+"/"+login.value);
		let ws = new WebSocket("ws://" + document.location.hostname + ":" + document.location.port + "/DM2_SallesDeChat/chatRooms/" + chatRoomName.value + "/" + login.value);

		console.log(login.value);
		console.log(chatRoomName.value);

		let txtHistory = document.getElementById("history");
		let txtMessage = document.getElementById("txtMessage");
		txtMessage.focus();


		ws.addEventListener("open", function(evt) {
			console.log("Connection");

		});


		ws.addEventListener("message", function(evt) {
			let message = evt.data;
			console.log("Receive new message: " + message);
			txtHistory.value += message + "\n";
		});

		ws.addEventListener("close", function(evt) {
			console.log("Connection closed ");
		});

		let btnSend = document.getElementById("btnSend");
		btnSend.addEventListener("click", function(clickEvent) {
			ws.send(txtMessage.value);
		});

		let btnClose = document.getElementById("btnClose");
		btnClose.addEventListener("click", function(clickEvent) {

			ws.close();

		});

		let interval = setInterval(function() {
			let currentDate = new Date();
			let dt = new Date(mydate);

			if (currentDate.getTime() > dt.getTime()) {
				console.log("Current date");
				console.log(currentDate);
				console.log("End date");
				console.log(dt);
				ws.close();
				console.log("closed connection!!!!");
				closedWebSocket = true;
				alert("Closed session");
				clearInterval(interval);

			}
		}, 10000);

	});

