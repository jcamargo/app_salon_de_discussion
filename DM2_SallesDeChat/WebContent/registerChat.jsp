<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="./assets/css/login.css" type="text/css">
<title>Insert chat room</title>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="mychatRooms.jsp">My
					chat rooms</a></li>
			<li class="nav-item"><a class="nav-link" href="invitedRooms.jsp">My
					invitations</a></li>
			<li class="nav-item"><a class="nav-link" href="registerChat.jsp">Create
					chat room</a></li>
			<li class="nav-item"><a class="nav-link" href="manageUsers.jsp">Manage
					users</a></li>

		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<form action="disconnection" method="POST">
						<input type="submit" class="btn btn-danger" value="Log out">
					</form>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container-fluid pt-4">
		<div class="row justify-content-center align-items-center h-100">
			<div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
				<form action="AddChatRoom" method="post">
					<div class="form-group">
						<label> Chat Room Name </label> <input class="form-control"
							type="text" name="chatRoomName">
					</div>
					<div class="form-group">
						<label>Chat Description </label> <input class="form-control"
							type="text" name="chatDescription">
					</div>
					<input  type="hidden" name="duration" value="10">


					<div class="form-group">
						<label> Date chat room</label> <input class="form-control"
							type="date" name="date">
					</div>
					<div class="form-group">
						<label> Begin time </label> <input class="form-control"
							type="time" name="startTime">
					</div>
					<div class="form-group">
						<label> End time </label> <input class="form-control" type="time"
							name="endTime">
					</div>
					<div class="form-group">
						<br> <input type="submit" class="btn btn-primary"
							value="Send data">
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>