<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="database.tools.CDaoUsers,models.User"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="./assets/css/login.css" type="text/css">
<title>Modify user</title>
</head>
<body>

	<%
	String emailUser = request.getParameter("userEmail");
	CDaoUsers daoUsers = new CDaoUsers();
	User user = daoUsers.getUserById(emailUser);
	%>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="mychatRooms.jsp">My
					chat rooms</a></li>
			<li class="nav-item"><a class="nav-link" href="invitedRooms.jsp">My
					invitations</a></li>
			<li class="nav-item"><a class="nav-link" href="registerChat.jsp">Create
					chat room</a></li>
			<%
			if (session.getAttribute("isAdmin").equals("true")) {
				out.println("<li class=\"nav-item\"><a class=\"nav-link\" href=\"manageUsers.jsp\">Manage users</a></li>");
			}
			%>


		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<form action="disconnection" method="POST">
						<input type="submit" class="btn btn-danger" value="Log out">
					</form>
				</li>
			</ul>
		</div>
	</nav>

	<div class="container-fluid pt-4">
		<div class="row justify-content-center align-items-center h-100">
			<div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
				<form action="modifyuser" method="post">
					<div class="form-group">
						<%
						out.println("<label>First Name</label> <input type=\"text\" class=\"form-control\"");
						out.println("name=\"name\" id=\"User first name\" value=\"" + user.getName() + "\">");
						%>
					</div>
					<div class="form-group">
						<%
						out.println("<label>Family Name</label> <input type=\"text\" class=\"form-control\"");
						out.println("name=\"sourcename\" id=\"User family name\" value=\"" + user.getFamilyName() + "\">");
						%>
					</div>
					<div class="form-group">
						<%
						out.println("<label> Email</label> <input type=\"email\" class=\"form-control\"");
						out.println("name=\"email\" id=\"User email\" value=\"" + user.getEmail() + "\" readonly>");
						%>
					</div>
					<div class="form-group">
						<%
						out.println("<label> Password</label> <input type=\"text\"");
						out.println("class=\"form-control\" name=\"pws\" id=\"User password\" value=\"" + user.getPassword() + "\">");
						%>
					</div>
					<label> Role </label><br>
					<div class="form-check-inline">

						<%
						out.println("<label class=\"form-check-label\"><input type=\"radio\"");
						if (user.getRole().equals("admin")) {
							out.println("class=\"form-check-input\" name=\"role\" value=\"admin\" checked>");

						} else {
							out.println("class=\"form-check-input\" name=\"role\" value=\"admin\" >");
						}
						out.println("Admin</label>");
						%>




					</div>
					<div class="form-check-inline">
						<%
						out.println("<label class=\"form-check-label\"><input type=\"radio\"");
						if (user.getRole().equals("other")) {
							out.println("class=\"form-check-input\" name=\"role\" value=\"other\" checked>");

						} else {
							out.println("class=\"form-check-input\" name=\"role\" value=\"other\" >");
						}
						out.println("Other</label>");
						%>
					</div>
					<br> <label> Gender </label><br>
					<div class="form-check-inline">

						<%
						out.println("<label class=\"form-check-label\"><input type=\"radio\"");
						if (user.getGender().equals("male")) {
							out.println("class=\"form-check-input\" name=\"gender\" value=\"male\" checked>");

						} else {
							out.println("class=\"form-check-input\" name=\"gender\" value=\"other\" >");
						}
						out.println("Male</label>");
						%>

					</div>
					<div class="form-check-inline">
						<%
						out.println("<label class=\"form-check-label\"><input type=\"radio\"");
						if (user.getGender().equals("female")) {
							out.println("class=\"form-check-input\" name=\"gender\" value=\"female\" checked>");

						} else {
							out.println("class=\"form-check-input\" name=\"gender\" value=\"female\" >");
						}
						out.println("Female</label>");
						%>

					</div>
					<div class="form-group">
						<br> <input type="submit" class="btn btn-primary"
							value="Modify"> <br>
					</div>
				</form>

			</div>
		</div>
	</div>

</body>
</html>