<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="java.util.*,java.sql.ResultSet,database.tools.CDaoUsers"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Invite Users</title>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="mychatRooms.jsp">My
					chat rooms</a></li>
			<li class="nav-item"><a class="nav-link" href="invitedRooms.jsp">My
					invitations</a></li>
			<li class="nav-item"><a class="nav-link" href="registerChat.jsp">Create
					chat room</a></li>
			<%
			if (session.getAttribute("isAdmin").equals("true")) {
				out.println("<li class=\"nav-item\"><a class=\"nav-link\" href=\"manageUsers.jsp\">Manage users</a></li>");
			}
			%>


		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<form action="disconnection" method="POST">
						<input type="submit" class="btn btn-danger" value="Log out">
					</form>
				</li>
			</ul>
		</div>
	</nav>

	<%
	String chatRoomNum = request.getParameter("currentRoomChat");
	CDaoUsers daoUsers = new CDaoUsers();
	int currentUser = 0;
	ResultSet rs = daoUsers.getUsersNotInvitedYet(chatRoomNum,String.valueOf(session.getAttribute("userEmail")));
	%>
	<div class="card">
		<div class="card-header">
			<h2>Users don�t invited yet</h2>
		</div>


		<div class="card-body">

			<form action="addinvited" method="post">
				<table class="table-striped table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Surname</th>
							<th>Email</th>
						</tr>
					</thead>

					<%
					while (rs.next()) {
						out.println("<tr>");
						out.println("<td>" + rs.getString("name") + "</td>");
						out.println("<td>" + rs.getString("sourname") + "</td>");
						out.println("<td>" + rs.getString("email") + "</td>");
						out.println("<td><input type=\"checkbox\" name=\"guesst" + currentUser + "\" value=\"" + rs.getString("email")
						+ "\" > </td>");
						out.println("</tr>");
						currentUser++;
					}
					%>


				</table>
				<input type="hidden" value="<%=chatRoomNum%>" name="chatRoomNum"><br>
				<input type="submit" class="btn btn-primary" value="Invite">
			</form>
		</div>
	</div>


</body>
</html>