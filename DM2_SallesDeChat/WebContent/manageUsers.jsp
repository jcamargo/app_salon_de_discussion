<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="database.tools.CDaoUsers,models.User,java.util.List"%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<meta charset="ISO-8859-1">
<title>Manage users</title>
</head>
<body>


	<%
	CDaoUsers daoUsers = new CDaoUsers();
	List<User> listUsers = daoUsers.getAllUsers();
	%>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="mychatRooms.jsp">My
					chat rooms</a></li>
			<li class="nav-item"><a class="nav-link" href="invitedRooms.jsp">My
					invitations</a></li>
			<li class="nav-item"><a class="nav-link" href="registerChat.jsp">Create
					chat room</a></li>
			<%
			if (session.getAttribute("isAdmin").equals("true")) {
				out.println("<li class=\"nav-item\"><a class=\"nav-link\" href=\"manageUsers.jsp\">Manage users</a></li>");
			}
			%>


		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<form action="disconnection" method="POST">
						<input type="submit" class="btn btn-danger" value="Log out">
					</form>
				</li>
			</ul>
		</div>
	</nav>
	<div class="card">
		<div class="card-header"><h2>Current users </h2></div>
		<div class="card-body">
			<table class="table-striped table-hover">
				<thead class="thead-dark">
					<tr>
						<th>Name</th>
						<th>Family name</th>
						<th>Email</th>
						<th>Role</th>
						<th>Gender</th>
					</tr>
				</thead>

				<%
				for (User user : listUsers) {
					out.println("<tr>");
					out.println("<td>" + user.getName() + "</td>");
					out.println("<td>" + user.getFamilyName() + "</td>");
					out.println("<td>" + user.getEmail() + "</td>");
					out.println("<td>" + user.getRole() + "</td>");
					out.println("<td>" + user.getGender() + "</td>");
					out.println(
					"<td><form action=\"modifyUsers.jsp\" method=\"POST\"><input class=\"btn btn-secondary\" type=\"submit\" value=\"Modify user\">");
					out.println("<input type=\"hidden\" name=\"userEmail\" value=\"" + user.getEmail() + "\"></form></td>");
					out.println(
					"<td><form action=\"deleteuser\" method=\"POST\"><input class=\"btn btn-warning\" type=\"submit\" value=\"Delete user\">");
					out.println("<input type=\"hidden\" name=\"userEmail\" value=\"" + user.getEmail() + "\"></form></td>");

					out.println("</tr>");
				}
				%>

			</table>

		</div>
	</div>

</body>
</html>