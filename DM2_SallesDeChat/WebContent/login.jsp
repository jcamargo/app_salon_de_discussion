<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="./assets/css/login.css" type="text/css">
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark navbar-me" >
   <a class="navbar-brand" href="#">UTC</a>
</nav>

<div class="container-fluid pt-4">
    <div class="row justify-content-center align-items-center h-100">
        <div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
<form action="connection" method="POST" name="connectinForm">
  <div class="form-group">
    <label >Email address</label>
    <input type="email" name="email" class="form-control" aria-describedby="emailHelp">
  </div>
  <div class="form-group">
    <label>Password</label>
    <input type="password" name="psw" class="form-control" id="exampleInputPassword1">
  </div>
	<input type="submit" value="Sign in" class="btn btn-primary">

</form>
<br>
<a href="registerUser.jsp" > Register</a>
        </div>
    </div>
</div>







</body>
</html>