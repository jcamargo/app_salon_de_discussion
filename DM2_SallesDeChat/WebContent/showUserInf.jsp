<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="database.tools.CDAOChats,models.CChat,java.util.List"%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>

	<%
	CDAOChats daoChats = new CDAOChats();
	List<CChat> rs = daoChats.getAllMyChatRooms(String.valueOf(request.getSession().getAttribute("userEmail")));
	%>
	<%
	List<CChat> rs1 = daoChats.getChatRoomsInvited(String.valueOf(request.getSession().getAttribute("userEmail")));
	%>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand -->
  <a class="navbar-brand" href="#">UTC</a>

  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="#">My chat rooms</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">My invitations</a>
    </li>
        <li class="nav-item">
                <a class="nav-link" href="#">Create chat room</a>
    </li>
        <li class="nav-item">
                <a class="nav-link" href="#">Manage users</a>
    </li>

  </ul>
</nav>
	<div class="card">
		<div class="card-header">Welcome <%=session.getAttribute("userEmail") %></div>
		<div class="card-body">
			<h5 class="card-title">My chat rooms</h5>
			<table class="table-striped table-hover">
				<thead class="thead-dark">
					<tr>
						<th>Room Chat Id</th>
						<th>Room Chat Name</th>
						<th>Description</th>
						<th>Date</th>
						<th>Duration</th>
						<th>Begins</th>
						<th>Ends</th>
					</tr>
				</thead>

				<%
				for (CChat chat:rs) {
					out.println("<tr>");
					out.println("<td>" + String.valueOf(chat.getIdChat()) + "</td>");
					out.println("<td>" + chat.getTitleSalle() + "</td>");
					out.println("<td>" + chat.getDescriptionSalle() + "</td>");
					out.println("<td>" + chat.getDate() + "</td>");
					out.println("<td>" + chat.getDuree() + "</td>");
					out.println("<td>" + chat.getStartTime() + "</td>");
					out.println("<td>" + chat.getEndTime() + "</td>");
					out.println(
					"<td><form action=\"inviteUsers.jsp\" method=\"POST\"><input class=\"btn btn-secondary\" type=\"submit\" value=\"Invite users\">");
					out.println("<input type=\"hidden\" name=\"currentRoomChat\" value=\"" + String.valueOf(chat.getIdChat()) + "\"></form></td>");
					out.println(
					"<td><form action=\"deletechatroom\" method=\"POST\"><input class=\"btn btn-warning\" type=\"submit\" value=\"Delete chat\">");
					out.println("<input type=\"hidden\" name=\"chatId\" value=\"" +String.valueOf(chat.getIdChat()) + "\"></form></td>");
					out.println(
					"<td><form action=\"clientPage.jsp\" method=\"post\"><input class=\"btn btn-success\" type=\"submit\" value=\"Join to room\">"
							+ "<input type=\"hidden\" name=\"chatRoomName\" value=\"" + chat.getTitleSalle() + "\"> "
							+ "<input type=\"hidden\" name=\"login\" value=\""
							+ String.valueOf(request.getSession().getAttribute("userEmail")) + "\"></form> " + "</td>");
					out.println("</tr>");

				}
				%>

			</table>
			<br>
			<h5 class="card-title">My invitations</h5>
			<table class="table-striped table-hover">
				<thead class="thead-dark">
					<tr>
						<th>Room Chat Id</th>
						<th>Room Chat Name</th>
						<th>Description</th>
						<th>Date</th>
						<th>Duration</th>
						<th>Begins</th>
						<th>Ends</th>
					</tr>
				</thead>

				<%
				for (CChat chat:rs1) {
					out.println("<td>" + String.valueOf(chat.getIdChat()) + "</td>");
					out.println("<td>" + chat.getTitleSalle() + "</td>");
					out.println("<td>" + chat.getDescriptionSalle() + "</td>");
					out.println("<td>" + chat.getDate() + "</td>");
					out.println("<td>" + chat.getDuree() + "</td>");
					out.println("<td>" + chat.getStartTime() + "</td>");
					out.println("<td>" + chat.getEndTime() + "</td>");
					out.println(
					"<td><form action=\"clientPage.jsp\" method=\"post\"><input class=\"btn btn-success\" type=\"submit\" value=\"Join to room\">"
							+ "<input type=\"hidden\" name=\"chatRoomName\" value=\"" + chat.getTitleSalle() + "\"> "
							+ "<input type=\"hidden\" name=\"login\" value=\""
							+ String.valueOf(request.getSession().getAttribute("userEmail")) + "\"></form> " + "</td>");
					out.println("</tr>");

				}
				%>

			</table>
			<br> <a href="registerChat.jsp" class="btn btn-primary">
				Create new chat room</a><br>

			<%
			if (session.getAttribute("isAdmin").equals("true")) {
				out.println("<br>");
				out.println("<a href=\"manageUsers.jsp\" class=\"btn btn-primary\"> Manage users </a><br>");
			}
			%>

			<br>
			<form action="disconnection" method="POST">
				<input type="submit" class="btn btn-danger" value="Log out">
			</form>
		</div>
	</div>



</body>
</html>