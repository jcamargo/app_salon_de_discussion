<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="database.tools.CDAOChats,models.CChat,java.util.List,
	java.util.Date,java.text.DateFormat,java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<meta charset="ISO-8859-1">
<title>My chat rooms</title>
</head>
<body>

	<%
	CDAOChats daoChats = new CDAOChats();
	List<CChat> rs = daoChats.getAllMyChatRooms(String.valueOf(request.getSession().getAttribute("userEmail")));
	%>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="mychatRooms.jsp">My
					chat rooms</a></li>
			<li class="nav-item"><a class="nav-link" href="invitedRooms.jsp">My
					invitations</a></li>
			<li class="nav-item"><a class="nav-link" href="registerChat.jsp">Create
					chat room</a></li>
			<%
			if (session.getAttribute("isAdmin").equals("true")) {
				out.println("<li class=\"nav-item\"><a class=\"nav-link\" href=\"manageUsers.jsp\">Manage users</a></li>");
			}
			%>


		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<form action="disconnection" method="POST">
						<input type="submit" class="btn btn-danger" value="Log out">
					</form>
				</li>
			</ul>
		</div>
	</nav>
	<div class="card">
		<div class="card-header">
			<h2>
				Welcome
				<%=session.getAttribute("userFamilyName")%>,<%=session.getAttribute("userName")%>
			</h2>
		</div>
		<div class="card-body">
			<h5 class="card-title">My chat rooms</h5>
			<table class="table-striped table-hover">
				<thead class="thead-dark">
					<tr>
						<th>Chat Id</th>
						<th>Chat room Name</th>
						<th>Description</th>
						<th>Date</th>
						<th>Begins</th>
						<th>Ends</th>
					</tr>
				</thead>

				<%
				for (CChat chat : rs) {
					String startTime = chat.getStartTime();
					String sDate1 = chat.getDate();
					String endTime = chat.getEndTime();
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					Date dateStart = (Date) formatter.parse(sDate1 + " " + startTime);
					Date dateEnd = (Date) formatter.parse(sDate1 + " " + endTime);
					System.out.println(new Date());
					System.out.println(dateStart);
					System.out.println(dateEnd);
					if (!new Date().after(dateEnd)) {
						out.println("<tr>");
						out.println("<td>" + String.valueOf(chat.getIdChat()) + "</td>");
						out.println("<td>" + chat.getTitleSalle() + "</td>");
						out.println("<td>" + chat.getDescriptionSalle() + "</td>");
						out.println("<td>" + chat.getDate() + "</td>");
						out.println("<td>" + chat.getStartTime() + "</td>");
						out.println("<td>" + chat.getEndTime() + "</td>");
						out.println(
						"<td><form action=\"inviteUsers.jsp\" method=\"POST\"><input class=\"btn btn-secondary\" type=\"submit\" value=\"Invite users\">");
						out.println("<input type=\"hidden\" name=\"currentRoomChat\" value=\"" + String.valueOf(chat.getIdChat())
						+ "\"></form></td>");
						out.println(
						"<td><form action=\"modifyChatRoom.jsp\" method=\"POST\"><input class=\"btn btn-secondary\" type=\"submit\" value=\"Modify chat\">");
						out.println("<input type=\"hidden\" name=\"currentRoomChat\" value=\"" + String.valueOf(chat.getIdChat())
						+ "\"></form></td>");
						out.println(
						"<td><form action=\"deletechatroom\" method=\"POST\"><input class=\"btn btn-warning\" type=\"submit\" value=\"Delete chat\">");
						out.println("<input type=\"hidden\" name=\"chatId\" value=\"" + String.valueOf(chat.getIdChat())
						+ "\"></form></td>");
						if (new Date().after(dateStart)) {
					out.println(
							"<td><form action=\"clientPage.jsp\" method=\"post\"><input class=\"btn btn-success\" type=\"submit\" value=\"Join to room\">"
									+ "<input type=\"hidden\" name=\"chatRoomName\" value=\"" + chat.getTitleSalle() + "\"> "
									+ "<input type=\"hidden\" name=\"login\" value=\""
									+ String.valueOf(request.getSession().getAttribute("userEmail")) + "\">"
									+ "<input type=\"hidden\" name=\"dateChat\" value=\"" + chat.getDate() + "\"> "
									+ "<input type=\"hidden\" name=\"endTimeChat\" value=\"" + chat.getEndTime() + "\"> "
									+ "</form> " + "</td>");

					out.println("</tr>");
						} else {
					out.println(
							"<td><form action=\"clientPage.jsp\" method=\"post\"><input class=\"btn btn-success\" type=\"submit\" value=\"Join to room\" disabled>"
									+ "<input type=\"hidden\" name=\"chatRoomName\" value=\"" + chat.getTitleSalle() + "\"> "
									+ "<input type=\"hidden\" name=\"login\" value=\""
									+ String.valueOf(request.getSession().getAttribute("userEmail")) + "\">"
									+ "<input type=\"hidden\" name=\"dateChat\" value=\"" + chat.getDate() + "\"> "
									+ "<input type=\"hidden\" name=\"endTimeChat\" value=\"" + chat.getEndTime() + "\"> "
									+ "</form> " + "</td>");
					out.println("</tr>");
						}

					} else {
						System.out.println("Lo eliminamos");
					}
				}
				%>

			</table>


		</div>
	</div>

</body>
</html>