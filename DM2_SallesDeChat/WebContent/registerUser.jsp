<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="./assets/css/login.css" type="text/css">
<title>Register User</title>
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark navbar-me">
		<a class="navbar-brand" href="#">UTC</a>
	</nav>

	<div class="container-fluid pt-4">
		<div class="row justify-content-center align-items-center h-100">
			<div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
				<form action="adduser" method="post">
					<div class="form-group">
						<label>First Name</label> <input type="text" class="form-control"
							name="name" id="User first name">
					</div>
					<div class="form-group">
						<label>Family Name</label> <input type="text" class="form-control"
							name="sourcename" id="User family name">
					</div>
					<div class="form-group">
						<label> Email</label> <input type="email" class="form-control"
							name="email" id="User email">
					</div>
					<div class="form-group">
						<label> Password</label> <input type="password"
							class="form-control" name="pws" id="User password">
					</div>
					<label> Role </label><br>
					<div class="form-check-inline">

						<label class="form-check-label"><input type="radio"
							class="form-check-input" name="role" value="admin" checked>
							Admin</label>
					</div>
					<div class="form-check-inline">
						<label class="form-check-label"><input type="radio"
							class="form-check-input" name="role" value="other">
							Other</label>
					</div>
					<br> <label> Gender </label><br>
					<div class="form-check-inline">

						<label class="form-check-label"><input type="radio"
							class="form-check-input" name="gender" id="male" value="male"
							checked> Male</label>
					</div>
					<div class="form-check-inline">
						<label class="form-check-label"><input type="radio"
							class="form-check-input" name="gender" id="female" value="female">
							Female</label>
					</div>
					<div class="form-group">
						<br> <input type="submit" class="btn btn-primary"
							value="Send data"> <br>
					</div>
				</form>

			</div>
		</div>
	</div>

</body>
</html>