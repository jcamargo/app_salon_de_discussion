<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="server.Server" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">


<title>Chat room</title>
<script type="text/javascript" src="./assets/js/clientConnection.js"></script>
<script type="text/javascript" src="./assets/js/jquery.js"></script>
<script type="text/javascript">
	var chatName = document.getElementById("chatRoomName");
	var loginUser = document.getElementById("login");
	var auto = setInterval(function() {
		$('#listConnectedUsers').load('connectedUsers.jsp').fadeIn("slow");
	}, 3000);
</script>
</head>
<body>


	<%
	out.println("<input id=\"chatRoomName\" value=\"" + request.getParameter("chatRoomName") + "\" type=\"hidden\"/>");
	out.println("<input id=\"login\" value=\"" + request.getParameter("login") + "\" type=\"hidden\"/>");
	out.println("<input id=\"dateChat\" value=\"" + request.getParameter("dateChat") + "\" type=\"hidden\"/>");
	out.println("<input id=\"endTimeChat\" value=\"" + request.getParameter("endTimeChat") + "\" type=\"hidden\"/>");
	%>
	<%
	String chatRoomNameV = request.getParameter("chatRoomName");
	session.setAttribute("currentChat", chatRoomNameV);
	%>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="mychatRooms.jsp">My
					chat rooms</a></li>
			<li class="nav-item"><a class="nav-link" href="invitedRooms.jsp">My
					invitations</a></li>
			<li class="nav-item"><a class="nav-link" href="registerChat.jsp">Create
					chat room</a></li>
			<%
			if (session.getAttribute("isAdmin").equals("true")) {
				out.println("<li class=\"nav-item\"><a class=\"nav-link\" href=\"manageUsers.jsp\">Manage users</a></li>");
			}
			%>


		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<form action="disconnection" method="POST">
						<input type="submit" class="btn btn-danger" value="Log out">
					</form>
				</li>
			</ul>
		</div>
	</nav>


	<div class="card">
		<div class="card-header">
			<h2>
				Chat room: <%=chatRoomNameV%>
			</h2>
		</div>
		<div class="card-body">

			<div class="row">
				<div class="col-sm-8">
					<div class="row">
						<textarea id="history" class="form-control" rows="13" readonly></textarea>
					</div>
					<br>
					<div class="row">
						<input id="txtMessage" type="text" class="col-6" />
						<button id="btnSend" class="col">Send message</button>
						<button id="btnClose" class="col">Close connection</button>
					</div>

				</div>
				<div class="col-sm-4">
					<div class="card">
						<div class="card-header">Connected users</div>
						<div class="card-body">
							<div id="listConnectedUsers"></div>
						</div>
					</div>

				</div>
			</div>






		</div>
	</div>










</body>
</html>