
CREATE TABLE `users` (
  `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sourname` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role` varchar(45) NOT NULL,
  `password` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gender` varchar(45) NOT NULL,
  PRIMARY KEY (`email`)
);

CREATE TABLE `chatrooms` (
    `idChat` int(11)  AUTO_INCREMENT,
   `email` varchar(200)  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `titleSalle` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descriptionSalle` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `startTime` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `endTime` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date` varchar(45) NOT NULL,
  `duration` int(11) NOT NULL,
  `startTime` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `endTime` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
  PRIMARY KEY (`idChat`),
  FOREIGN KEY (`email`) REFERENCES users(`email`) ON DELETE CASCADE
);

CREATE TABLE `userInvitedRooms` (
	`email` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `idChat` int(11) NOT NULL,
    FOREIGN KEY (`email`) REFERENCES users(`email`) ON DELETE CASCADE,
    FOREIGN KEY (`idChat`) REFERENCES chatrooms(`idChat`) ON DELETE CASCADE
);


/*Query pour retourner les utilisateurs qui ne sont pas encore invités à notre salon de discussion.
Il faut realiser quelques test sur il*/
/*SELECT * from users a 
LEFT JOIN userInvitedRooms c on c.email=a.email and c.idChat=1   
where c.email is null and c.idChat is null and  a.email !='aa@gmail.com' ;*/
